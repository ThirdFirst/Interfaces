/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thirdfirst;
import java.util.Date;

/**
 *
 * @author User
 */
public class Servicio {
    private int idServicio;
    private int expNecesaria;
    private String profnecesario;
    private boolean enCurso;
    private String descripcion;
    private Date fechaInicio=new Date();

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public int getIdServicio() {
        return idServicio;
    }
    
    public Date getFechaInicio() {
        return fechaInicio;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    public boolean enCurso(){
        return enCurso;
    }

    public void setEnCurso(boolean enCurso) {
        this.enCurso = enCurso;
    }

    public void setProfnecesario(String profnecesario) {
        this.profnecesario = profnecesario;
    }

    public String getProfnecesario() {
        return profnecesario;
    }

    public void setExpNecesaria(int expNecesaria) {
        this.expNecesaria = expNecesaria;
    }

    public int getExpNecesaria() {
        return expNecesaria;
    }

}
