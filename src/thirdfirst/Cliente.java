/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thirdfirst;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author User
 */
public class Cliente {
    private int idCliente;
    private String nombre;
    private int numeroTelefono;
    ConeccionBaseDatos conectar = new ConeccionBaseDatos();
    Connection sn = conectar.conexion();
        
    public Cliente(int idCliente, String nombre, int numeroTelefono){
        this.idCliente=idCliente;
        this.nombre=nombre;
        this.numeroTelefono=numeroTelefono;
    }
    
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public void setNumeroTelefono(int numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public int getNumeroTelefono() {
        return numeroTelefono;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    public void solicitarServicio(int idServicio, int expNecesaria, String profNecesario, boolean enCurso, String Descripcion) throws SQLException{
        Date date = new Date();
        PreparedStatement query = sn.prepareStatement("INSERT INTO servicio(idServicio, expNecesaria, profNecesario, enCurso, Descripcion, fechaInicio) VALUES (?,?,?,?,?,now())");
        query.setInt(1, idServicio);
        query.setInt(2, expNecesaria);
        query.setString(3, profNecesario);
        query.setBoolean(4, enCurso);
        query.setString(5,Descripcion);
        query.executeUpdate();
    }
}
