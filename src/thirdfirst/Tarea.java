/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thirdfirst;

/**
 *
 * @author User
 */
public class Tarea {
    
    private int idTarea;
    private String tarea;
    private boolean realizada;

    public boolean getRealizada(){
        return realizada; 
    }
    
    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public String getTarea() {
        return tarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public int getIdTarea() {
        return idTarea;
    }
}
