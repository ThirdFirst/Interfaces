/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thirdfirst;

import java.util.ArrayList;

public class Profesional {
    private int idProfesional;
    private String nombre;
    private int numeroTelefono;
    private String calificacion;
    private boolean enServicio;
    private String experiencia;
    
    private ArrayList <Profesion> Profesiones= new ArrayList <Profesion> ();
    
    public int getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(int idProfesional) {
        this.idProfesional = idProfesional;
    }
    
    public ArrayList<Profesion> getProfesiones(){
        return Profesiones;
    }
    public void ingresarProfesion(Profesion profesion){
        Profesiones.add(profesion);
    }
    public void setExperiencia(String experiencia) {
        this.experiencia = experiencia;
    }

    public String getExperiencia() {
        return experiencia;
    }
    public boolean getEnSevicio(){
        return enServicio;
    }

    public void setEnServicio(boolean enServicio) {
        this.enServicio = enServicio;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public int getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(int numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
