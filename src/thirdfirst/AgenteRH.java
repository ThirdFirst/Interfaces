
package thirdfirst;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AgenteRH extends Agente{
    private String cargo;
    ConeccionBaseDatos conectar = new ConeccionBaseDatos();
    Connection sn = conectar.conexion();
    
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCargo() {
        return cargo;
    }
    
    public void establecerNuevosRegistros(){
        
    }
    
    public void asignarProfesional(int idProfesional, int idServicio){
        try {
            PreparedStatement query = sn.prepareStatement("UPDATE profesional SET enServicio=1 WHERE idProfesional='"+idProfesional+"'");
            query.executeUpdate();
            PreparedStatement query2 = sn.prepareStatement("UPDATE servicio SET idProfesional='"+idProfesional+"' WHERE idServicio='"+idServicio+"'");
            query2.executeUpdate();
            PreparedStatement query3 = sn.prepareStatement("UPDATE servicio SET enCurso=1 WHERE idServicio='"+idServicio+"'");
        } catch (SQLException ex) {
            Logger.getLogger(AgenteRH.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sacarPersonal(Profesional profesional){
        
    }
    
    public void mantenerServicio(){
    
    }
    
    public void clasificarPersonal(){
    
    }
    
    public void verificarDisponibilidadP(){
        
    }
    
    public void verificarRequisitosP(){
        
    }
    
    public void clasificarProyectos(Servicio servicio){
        
    }
    
    public void confirmarRequisitos(){
    
    }
}
