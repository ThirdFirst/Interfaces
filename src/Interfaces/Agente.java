/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import interfaz.tickets;
import javax.swing.JOptionPane;
import thirdfirst.Cliente;
import thirdfirst.ThirdFirst;

/**
 *
 * @author User
 */
public class Agente extends javax.swing.JFrame {

    /**
     * Creates new form Inicial
     */
    public Agente() {
        initComponents();
        this.setLocationRelativeTo(null);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ButtonClose = new javax.swing.JButton();
        jLabelLogo = new javax.swing.JLabel();
        ButtonProfesionales = new javax.swing.JToggleButton();
        ButtonServicios = new javax.swing.JToggleButton();
        ButtonClientes = new javax.swing.JToggleButton();
        libre = new javax.swing.JToggleButton();
        BAck = new javax.swing.JToggleButton();
        jLabelContraste = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ButtonClose.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        ButtonClose.setText("X");
        ButtonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonCloseActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 10, 50, 30));

        jLabelLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/THIRDFIST.png"))); // NOI18N
        getContentPane().add(jLabelLogo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, -1, -1));

        ButtonProfesionales.setText("Profesionales");
        ButtonProfesionales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonProfesionalesActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonProfesionales, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 170, 60));

        ButtonServicios.setText("Servicios");
        ButtonServicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonServiciosActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonServicios, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 200, 180, 60));

        ButtonClientes.setText("Clientes");
        ButtonClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonClientesActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonClientes, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, 170, 70));

        libre.setText("Tickets");
        libre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                libreActionPerformed(evt);
            }
        });
        getContentPane().add(libre, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 300, 180, 70));

        BAck.setText("Atrás");
        BAck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BAckActionPerformed(evt);
            }
        });
        getContentPane().add(BAck, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 10, -1, 30));

        jLabelContraste.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ImgenenFondo.jpg"))); // NOI18N
        jLabelContraste.setPreferredSize(new java.awt.Dimension(800, 600));
        getContentPane().add(jLabelContraste, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ButtonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_ButtonCloseActionPerformed

    private void ButtonServiciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonServiciosActionPerformed
        Servicios ventana = new Servicios();
        ventana.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ButtonServiciosActionPerformed

    private void ButtonClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonClientesActionPerformed
        Clientes ventana = new Clientes();
        ventana.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ButtonClientesActionPerformed

    private void ButtonProfesionalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonProfesionalesActionPerformed
        Profesionales pro = new Profesionales();
        pro.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ButtonProfesionalesActionPerformed

    private void BAckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BAckActionPerformed
        Inicial ini = new Inicial();
        ini.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_BAckActionPerformed

    private void libreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_libreActionPerformed
        tickets nuevo = new tickets();
        nuevo.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_libreActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Agente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Agente().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton BAck;
    private javax.swing.JToggleButton ButtonClientes;
    private javax.swing.JButton ButtonClose;
    private javax.swing.JToggleButton ButtonProfesionales;
    private javax.swing.JToggleButton ButtonServicios;
    private javax.swing.JLabel jLabelContraste;
    private javax.swing.JLabel jLabelLogo;
    private javax.swing.JToggleButton libre;
    // End of variables declaration//GEN-END:variables

   
}
