
package interfaz;

import java.sql.PreparedStatement;


public class consulta {
    
    public boolean registrar(String Nombre, String Estado, String Descripcion, Integer Cantidad){
        PreparedStatement pst = null;
        
        try{
            String Consulta = "insert into Articulo(Nombre,Estado,Descripcion,Cantidad)values(?,?,?,?)";
           // pst = getConexion().prepareStatement(Consulta);
            pst.setString(1, Nombre);
            pst.setString(2, Estado);
            pst.setString(3, Descripcion);
            pst.setInt(4, Cantidad);
            
            if(pst.executeUpdate() == 1){
                return true;
            }
            
        }catch(Exception ex){
            System.err.println("Error"+ex);
        }finally{
            try{
               // if(getConexion() != null) getConexion().close();
                if(pst != null) pst.close();
        }catch(Exception e){
            System.err.println("Error"+e);
        }
        }
        
        
        
        return false;
        
    }
    
}
